# Belazar git

## Clone project
- Contoh clone project belazar-git
```
git clone https://gitlab.com/soevar/belazar-git
```

## Create branch
- Contoh buat brach baru 'fitur_baru' dari project belazar-git

Pastikan data sudah up to date
```
git pull
```

Buat branch di komputer lokal
```
git checkout -b fitur_baru
```

Cek semua branch di komputer lokal
```
git branch -a
```

Kalau mau push data branch 'fitur_baru' ke remote gitlab/github
``` 
git push origin fitur_baru
```

## Pindah Branch
``` 
git checkout master -> pindah ke branch master
git checkout fitur_baru -> pindah ke branch fitur_baru
```

## Merge Branch
Pastikan pointer berada pada branch master, agar bisa di merge. 
Misalkan kita ingin merge branch fitur_baru ke branch master.
```
git checkout master -> pointer branch master
git merge fitur_baru -> merge fitur_baru ke master
```

## Delete Branch
Pastikan pointer berada pada branch master, agar bisa di delete.
Misalkan kita ingin delete branch fitur_baru.
```
git checkout master -> pointer branch master
git branch -d fitur_baru
```


## Create Tag
Misalkan kita mau buat tag versi 0.1.0 dan disertai dengan commit pada project belazar-git
```
git tag -a 0.1.0 -m "Release version 0.1.0 module system"
```

Cek tag di komputer lokal
```
git tag -l -n3
```

Kalau mau push data tag ke remote gitlab/github
```
git push origin 0.1.0
```








